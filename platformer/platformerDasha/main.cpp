#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <string>
#include <iostream>
#include "Core.h"
#include "Player.h"
#include "Enemy.h"
#include "Menu.h"

using namespace sf;

Core Core1;
// f(five), F(FIFTY), E(ENEMY), e(exit), t(ten), B(BRICK), b(bush), c(cloud), G(Ground) �����, P ������, p(������ ������� ��������), S(����), H - �����
sf::String TileMap[Core::H] = {
	"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	"0                                                                                       c                                                            0",
	"0                                 c                       c            c             c                                                  c            0",
	"0                   c                                  c     c                   c                c                       c                          0",
	"0    c                    F             c                                                              c                                       c     0",
	"0                        ttt                                 f                         F      F               c                                      0",
	"0                      ffffff                                                        ftttf  ftttf                                                    0",
	"0                                                                                      B      B                                         S            0",
	"0                                          BBBBBBBBBB                    PPPPPPP                                                       BBB           0",
	"0                                          B        B                 PPPG     GPPPP                                                  BBBBB          0",
	"0               f    fBfBf             BBBBB        B             BBPPGG          GGPP                 B                             BBBBBBB         0",
	"0                                      B                          BB                                   B                             BHHBHHB         0",
	"0                                      B                          BB     F                             B                             BBBBBBB         0",
	"0     E  b HHHH  b  HH EH    E   p     B   b       p     E   b    BBE   ttt   b   HHH  b    E   p    b B  HHHHHHH     E b            BBBBBBB         0",
	"PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP",
	"GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",
	"GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",
};

int nEnemy = 0; //���-�� ������ ������������, �������

void drawMap(Menu &Menu1, Player &oPlayer, RenderWindow &window, Enemy * enemy)
{
	for (int i=0; i < Core1.H; i++)
		for (int j=0; j < Core1.W; j++)
		{
			if ((TileMap[i][j]==' ')||(TileMap[i][j]=='0')) continue;

			if (TileMap[i][j]=='P') { Menu1.sPGround.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sPGround); }

			if (TileMap[i][j]=='p') { Menu1.sPlant.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sPlant); }

			if (TileMap[i][j]=='B') { Menu1.sBrick.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sBrick); }

			if (TileMap[i][j]=='G') { Menu1.sGround.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sGround); }

			if (TileMap[i][j]=='b') { Menu1.sBush.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sBush); }

			if (TileMap[i][j]=='E' && nEnemy < Core1.kEnemies)
			{
				enemy[nEnemy].set(Menu1.tEnemyDead, Menu1.tEnemy, Menu1.tEnemyMirror, j * Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY + 4, 50, 28);
				enemy[nEnemy].sprite.setTextureRect(IntRect(104,120,45,30));//��������� ��������� �����
				nEnemy++;
			}

			if (TileMap[i][j]=='c') { Menu1.sCloud.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sCloud); }

			if (TileMap[i][j]=='S') { Menu1.sStag.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sStag); }

			if (TileMap[i][j]=='H') { Menu1.sFence.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.sFence); }

			if (TileMap[i][j]=='f') { Menu1.s5Gold.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.s5Gold); }

			if (TileMap[i][j]=='t') { Menu1.s10Gold.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.s10Gold); }

			if (TileMap[i][j]=='F') { Menu1.s50Gold.setPosition(j*Core1.TileSize - oPlayer.offsetX, i*Core1.TileSize - oPlayer.offsetY); window.draw(Menu1.s50Gold); }
		}

		window.draw(Menu1.moneyText);
		Menu1.counterText.setString( std::to_string(oPlayer.counter) );
		window.draw(Menu1.counterText);
		window.draw(Menu1.KilledText);
		Menu1.KilledcounterText.setString( std::to_string(Menu1.killedCounter) );
		window.draw(Menu1.KilledcounterText);

		for (int i = 0; i < 3; i++) { window.draw(Menu1.health[i]); }
}

int main()
{
	RenderWindow window(VideoMode(1000, 500), "My Game");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		Menu Menu1;

		Menu1.health[0].setPosition(5,1*Core1.TileSize);
		Menu1.health[1].setPosition(Core1.TileSize+10,1*Core1.TileSize);
		Menu1.health[2].setPosition(2*Core1.TileSize+15,1*Core1.TileSize);

		while(Menu1.bMenu)
		{
			window.clear(sf::Color::Black);

			Menu1.returnDefaultColor();

			if (sf::IntRect(Menu1.start.getPosition().x, Menu1.start.getPosition().y, 100, 40).contains(sf::Mouse::getPosition(window))) {Menu1.start.setOutlineColor(sf::Color::Yellow); Menu1.MenuNum=1;}
			if (sf::IntRect(Menu1.about.getPosition().x, Menu1.about.getPosition().y, 100, 40).contains(sf::Mouse::getPosition(window))) {Menu1.about.setOutlineColor(sf::Color::Yellow); Menu1.MenuNum=2;}
			if (sf::IntRect(Menu1.exit.getPosition().x, Menu1.exit.getPosition().y, 100, 40).contains(sf::Mouse::getPosition(window))) {Menu1.exit.setOutlineColor(sf::Color::Yellow); Menu1.MenuNum=3;}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (Menu1.MenuNum==1) { Menu1.bMenu = false; Menu1.bGame = true;}
				if (Menu1.MenuNum==2)
				{
					window.draw(Menu1.sBackground);
					window.draw(Menu1.ABOUT_BRIEFING);
					window.display();
					while(!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape));
				}
				if (Menu1.MenuNum==3)  { return 0; }
			}

			Menu1.MenuNum=0;
			Menu1.DrawMainMenu(window);
		}

		Player oPlayer(Menu1.tPlayer);
		Enemy enemy[Core1.kEnemies];
		nEnemy = 0;

		Clock clock;

		while (Menu1.bGame)
		{
			float time = clock.getElapsedTime().asMicroseconds();

			sf::Time frameTime = clock.restart();

			time = time/300;  // ����� ���������� �������� ����

			//! ��������� �������
			if (Keyboard::isKeyPressed(Keyboard::Left))    oPlayer.dx=-0.1;

			if (Keyboard::isKeyPressed(Keyboard::Right))    oPlayer.dx=0.1;

			if (Keyboard::isKeyPressed(Keyboard::Up))	if (oPlayer.onGround) { oPlayer.dy=-0.10; oPlayer.onGround=false; }

			oPlayer.update(time, TileMap);
			//---------------------------------------------------------

			for (int i = 0; i < Core1.kEnemies; i++)
			{
				enemy[i].update(time, oPlayer.offsetX, oPlayer.offsetY, TileMap);

				if  (oPlayer.rect.intersects( enemy[i].rect ))
				{
					if (enemy[i].life && oPlayer.sprite.getColor() != Color::Red)//���� ���� ��� � ���� ������ �� �������, ��
					{
						if (oPlayer.dy>0) { enemy[i].dx=0; oPlayer.dy=-0.2; enemy[i].life=false; Menu1.killedCounter++; enemy[i].sprite.setColor(sf::Color::White); } //���� �������� ������
						else { oPlayer.sprite.setColor(Color::Red); Menu1.health[oPlayer.hp].setTexture(Menu1.tEmptyHeart); oPlayer.hp--; }// ����� �������� ��
					}
				}
			}

			if (oPlayer.rect.left>500) oPlayer.offsetX = oPlayer.rect.left-500;           //! ��������, ��������� ��� ���������

			window.clear(Color(107,140,255));

			drawMap(Menu1, oPlayer, window, enemy);

			oPlayer.resurrect(time);

			window.draw(oPlayer.sprite);

			for (int i = 0; i < Core1.kEnemies; i++) { enemy[i].setDirection(window, frameTime); window.draw(enemy[i].sprite); }

			if (oPlayer.hp < 0) { Menu1.bGame = false; Menu1.endString = "YOU LOST :("; }
			else if (oPlayer.sprite.getPosition().x >= Menu1.sStag.getPosition().x && oPlayer.counter > 200) { Menu1.bGame = false;  Menu1.endString = "YOU WIN!!!"; }

			window.display();
		}

		Menu1.end.setString(Menu1.endString);

		while(!Keyboard::isKeyPressed(Keyboard::Escape))
		{
			window.clear(sf::Color::Black);
			window.draw(Menu1.moneyText);
			window.draw(Menu1.counterText);
			window.draw(Menu1.KilledText);
			window.draw(Menu1.KilledcounterText);
			window.draw(Menu1.end);
			window.display();
		}
		return 0;
	}
}