#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Core.h"

using namespace sf;

#pragma once

class Player: public Core
{
public:

	Player(Texture &image);

	float offsetX, offsetY;
	float dx,dy;
	FloatRect rect;
	bool onGround;
	Sprite sprite;
	float currentFrame;
	float resTime;
	int counter;
	int hp;

	void update(float time, sf::String * TileMap);
	void Collision(int num, sf::String * TileMap);
	void resurrect(float time);
};
