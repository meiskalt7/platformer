#include "Core.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#pragma once

class Menu//����� ����� GUI ��� ������� �������, ��� � �������
{
public:
	Menu(void);
	~Menu(void);

	int MenuNum;
	int killedCounter;
	int fontSize;
	bool bMenu;
	bool bGame;

	sf::Texture iBackground;
	sf::Sprite sBackground;

	sf::Font font;

	sf::Text moneyText, counterText, KilledText, KilledcounterText;

	sf::RectangleShape start, about, exit;
	sf::Text START_TEXT, ABOUT_TEXT, ABOUT_BRIEFING, EXIT_TEXT, LOADING_TEXT;

	std::string strAbout;

	sf::Texture tPlayer, tEnemy, tEnemyMirror, tEnemyDead, tCloud, tHeart, tEmptyHeart, tPGround, tGround, tBrick, tBush, tPlant, tStag, tFence;
	sf::Sprite sCloud, sGround, sPGround, sBrick, sBush, sPlant, sStag, sFence, health[3];

	sf::Texture t5Gold, t10Gold, t50Gold;
	sf::Sprite s5Gold, s10Gold, s50Gold;

	sf::Text end;
	sf::String endString;

	void DrawMainMenu(sf::RenderWindow &window);
	void returnDefaultColor();
};
