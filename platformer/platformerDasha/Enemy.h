#include "Core.h"
#include "Animation.hpp"
#include "AnimatedSprite.hpp"

using namespace sf;

#pragma once

class Enemy : public Core
{
public:
	float dx,dy;
	FloatRect rect;
	Sprite sprite; //������ ��������
	Animation aLEnemy, aREnemy;
	Animation* animationLeft;
	Animation* animationRight;
	AnimatedSprite animatedSprite;
	char aChar;
	float currentFrame;
	bool life;

	void set(Texture &imageDead,Texture &imageL, Texture &imageR, float rectLeft, float rectTop, float rectWidth, float rectHeight);

	void update(float time, float offsetX, float offsetY, sf::String * TileMap);

	void Collision(sf::String * TileMap);

	void setDirection(RenderWindow &window, sf::Time frameTime);
};
