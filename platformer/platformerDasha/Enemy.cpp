#include "Enemy.h"

void Enemy::set(Texture &imageDead,Texture &imageL, Texture &imageR, float rectLeft, float rectTop, float rectWidth, float rectHeight)
{
	sprite.setTexture(imageDead);
	sprite.setColor(sf::Color::Transparent); 

	rect = FloatRect(rectLeft, rectTop, rectWidth, rectHeight);

	aLEnemy.setSpriteSheet(imageL);
	aLEnemy.addFrame(sf::IntRect(0,125,50,28));
	aLEnemy.addFrame(sf::IntRect(50, 125, 50, 28));

	aREnemy.setSpriteSheet(imageR);
	aREnemy.addFrame(sf::IntRect(253, 125, 50, 28));
	aREnemy.addFrame(sf::IntRect(303, 125, 50,28));

	animationLeft = &aLEnemy;
	animationRight = &aREnemy;
	animatedSprite.setPosition(rectLeft, rectTop);

	dx=0.15;
	currentFrame = 0;
	life=true;
}

void Enemy::update(float time, float offsetX, float offsetY, sf::String * TileMap)
{
	rect.left += dx * time;

	Collision(TileMap);

	currentFrame += time * 0.0005;
	if (currentFrame > 2) currentFrame -= 2;

	// ��������� �������� � ����������� �� ������� ��������
	if (dx>0) { aChar = 'L'; }
	if (dx<0) { aChar = 'R'; }
	if (!life) { animatedSprite.setColor(sf::Color::Transparent); }
	sprite.setPosition(rect.left - offsetX, rect.top - offsetY); //��������� �� �����
	animatedSprite.setPosition(rect.left - offsetX, rect.top - offsetY);
}

void Enemy::Collision(sf::String * TileMap) //������������ �����
{
	for (int i = rect.top/TileSize ; i<(rect.top+rect.height)/TileSize; i++)
		for (int j = rect.left/TileSize; j<(rect.left+rect.width)/TileSize; j++)
			if ((TileMap[i][j]=='P') || (TileMap[i][j]=='0') || (TileMap[i][j]=='B'))
			{
				if (dx>0)
				{ rect.left =  j*TileSize - rect.width; dx*=-1; }
				else if (dx<0)
				{ rect.left =  j*TileSize + TileSize;  dx*=-1; }
			}
}

void Enemy::setDirection(RenderWindow &window, sf::Time frameTime)
{
	window.draw(sprite);
	if (aChar == 'R')
	{
		animatedSprite.play(*animationLeft);
	}
	else
	{
		animatedSprite.play(*animationRight);
	}

	animatedSprite.update(frameTime);
	window.draw(animatedSprite);
}