#include "Player.h"

Player::Player(Texture &image)
{
	sprite.setTexture(image);
	
	rect = FloatRect(3*TileSize,3*TileSize, 34, 48);
	offsetX=0;
	offsetY=0;
	dx=dy=0.1;
	currentFrame = 0;
	counter = 0;
	resTime = 0;
	hp = 2;
}

void Player::update(float time, sf::String * TileMap)
{
	rect.left += dx * time;
	Collision(0, TileMap);

	if (!onGround) dy=dy+0.00005*time;
	rect.top += dy*time;
	onGround=false;
	Collision(1, TileMap);

	currentFrame += time * 0.003;
	if (currentFrame > 5) currentFrame -= 5;

	if(dx > 0) sprite.setTextureRect(sf::IntRect(48*int(currentFrame)+48, 109,-48, 34));
	if(dx < 0) sprite.setTextureRect(sf::IntRect(48*int(currentFrame), 109, 48, 34));

	sprite.setPosition(rect.left - offsetX, rect.top - offsetY);

	dx=0;
}

void Player::Collision(int num, sf::String * TileMap)
{
	for (int i = rect.top/TileSize; i<(rect.top+rect.height)/TileSize; i++)
		for (int j = rect.left/TileSize; j<(rect.left+rect.width)/TileSize; j++)
		{
			if ((TileMap[i][j]=='P')  || (TileMap[i][j]=='0') ||  (TileMap[i][j]=='B') || (TileMap[i][j]=='G')) //Непроходимые блоки
			{
				if (dy>0 && num==1){ rect.top =   i*TileSize -  rect.height;  dy=0;   onGround=true; }
				if (dy<0 && num==1){ rect.top = i*TileSize + TileSize;   dy=0;}
				if (dx>0 && num==0){ rect.left =  j*TileSize -  rect.width; }
				if (dx<0 && num==0){ rect.left =  j*TileSize +TileSize;}
			}

			if (TileMap[i][j]=='f')
			{
				TileMap[i][j]=' ';  counter += 5;
			}
			if (TileMap[i][j]=='t')
			{
				TileMap[i][j]=' ';  counter += 10;
			}
			if (TileMap[i][j]=='F')
			{
				TileMap[i][j]=' ';  counter += 50;
			}
		}
}

void Player::resurrect(float time)
{
	if (sprite.getColor() == Color::Red)
	{
		resTime += time;
		if (resTime > 2000)
		{
			sprite.setColor(Color::White);
			resTime -= 2000;
		}
	} else { sprite.setColor(sf::Color::White); }
}