#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#pragma once

class Core
{
public:
	Core(void);
	~Core(void);

	static const int kEnemies = 7;

	static const int H = 17;
	static const int W = 150;

	static const int TileSize = 32;
};