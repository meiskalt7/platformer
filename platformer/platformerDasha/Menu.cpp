#include "Menu.h"
#include <string>

Menu::Menu(void)
{
	MenuNum = 0;
	killedCounter = 0;
	fontSize = 20;
	bMenu = true;
	bGame = true;

	iBackground.loadFromFile("Res/MainWall.png");
	sBackground.setTexture(iBackground);

	font.loadFromFile("Res/MAGIMTOS.ttf");

	moneyText.setString("Money = ");
	moneyText.setFont(font);
	moneyText.setColor(sf::Color::Green);
	moneyText.setPosition(130,32);

	counterText.setString("");
	counterText.setFont(font);
	counterText.setColor(sf::Color::Green);
	counterText.setPosition(280,32);

	KilledText.setString("Killed = ");
	KilledText.setFont(font);
	KilledText.setColor(sf::Color::Yellow);
	KilledText.setPosition(350,32);

	KilledcounterText.setString("");
	KilledcounterText.setFont(font);
	KilledcounterText.setColor(sf::Color::Yellow);
	KilledcounterText.setPosition(500,32);

	start.setFillColor(sf::Color::Red);
	start.setSize(sf::Vector2f(100, 40));
	start.setOutlineColor(sf::Color::Black);
	start.setOutlineThickness(5);
	start.setPosition(148, 365);

	about.setFillColor(sf::Color::Green);
	about.setSize(sf::Vector2f(100, 40));
	about.setOutlineColor(sf::Color::Black);
	about.setOutlineThickness(5);
	about.setPosition(465, 365);

	exit.setFillColor(sf::Color::Magenta);
	exit.setSize(sf::Vector2f(100, 40));
	exit.setOutlineColor(sf::Color::Black);
	exit.setOutlineThickness(5);
	exit.setPosition(730, 365);

	START_TEXT.setString("Start");
	START_TEXT.setFont(font);
	START_TEXT.setColor(sf::Color::Black);
	START_TEXT.setCharacterSize(fontSize);
	START_TEXT.setPosition(158, 375); //+20 + 10

	ABOUT_TEXT.setString("About");
	ABOUT_TEXT.setFont(font);
	ABOUT_TEXT.setColor(sf::Color::Black);
	ABOUT_TEXT.setCharacterSize(fontSize);
	ABOUT_TEXT.setPosition(475, 375);

	strAbout = "Game - Coursework\n To jump higher - jump on mobs\n Press ESC to return to the menu";
	ABOUT_BRIEFING.setString(strAbout);
	ABOUT_BRIEFING.setFont(font);
	ABOUT_BRIEFING.setColor(sf::Color::Black);
	ABOUT_BRIEFING.setCharacterSize(fontSize);
	ABOUT_BRIEFING.setPosition(450, 320);

	EXIT_TEXT.setString("Exit");
	EXIT_TEXT.setFont(font);
	EXIT_TEXT.setColor(sf::Color::Black);
	EXIT_TEXT.setCharacterSize(fontSize);
	EXIT_TEXT.setPosition(740, 375);

	end.setString("");
	end.setFont(font);
	end.setCharacterSize(100);
	end.setColor(sf::Color::Green);
	end.setPosition(180,200);

	t5Gold.loadFromFile("Res/5Gold32.png");
	t10Gold.loadFromFile("Res/10Gold.png");
	t50Gold.loadFromFile("Res/50Gold.png");

	tPlayer.loadFromFile("Res/hero_sprites.png");
	tEnemy.loadFromFile("Res/enemies_spritesheet.png");

	tEnemyMirror.loadFromFile("Res/enemies_spritesheetMirror.png");
	tEnemyDead.loadFromFile("Res/enemies_spritesheet.png");

	tCloud.loadFromFile("Res/cloud1.png");
	tHeart.loadFromFile("Res/hud_heartFull.png");
	tEmptyHeart.loadFromFile("Res/hud_heartEmpty.png");
	tStag.loadFromFile("Res/flagGreen2.png");
	tGround.loadFromFile("Res/grassCenter.png");
	tPGround.loadFromFile("Res/GrassBlock.png");
	tBrick.loadFromFile("Res/brickWall.png");
	tBush.loadFromFile("Res/bush.png");
	tPlant.loadFromFile("Res/plant.png");
	tFence.loadFromFile("Res/fence.png");

	sCloud.setTexture(tCloud);
	sGround.setTexture(tGround);
	sPGround.setTexture(tPGround);
	sBrick.setTexture(tBrick);
	sBush.setTexture(tBush);
	sPlant.setTexture(tPlant);
	sStag.setTexture(tStag);
	sFence.setTexture(tFence);

	for (int i = 0; i < 3; i++)
	{
		health[i].setTexture(tHeart);
	}

	s5Gold.setTexture(t5Gold);
	s10Gold.setTexture(t10Gold);
	s50Gold.setTexture(t50Gold);
}

Menu::~Menu(void)
{
}

void Menu::DrawMainMenu(sf::RenderWindow &window)
{
	window.draw(sBackground);
	window.draw(start);
	window.draw(about);
	window.draw(exit);
	window.draw(START_TEXT);
	window.draw(ABOUT_TEXT);
	window.draw(EXIT_TEXT);
	window.display();
}

void Menu::returnDefaultColor()
{
	start.setOutlineColor(sf::Color::Black);
	about.setOutlineColor(sf::Color::Black);
	exit.setOutlineColor(sf::Color::Black);
}